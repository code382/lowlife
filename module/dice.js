import { meleeContent, rangedContent } from "./dialog.js"

function meleeAttack (actorInfo, itemInfo) {
    let actor = actorInfo;
    let item = itemInfo;

    //Get useful stats from actor
    let strMod = actor.system.attributes.str.mod;
    let atkBon = actor.system.atkBonus;
    let name = actor.name;

    //Get useful stats from item
    let itemDmg = item.system.dmg;
    let itemNat = item.system.nat19;
    let itemNote = item.system.note;
    let itemData = {itemDmg, itemNat, itemNote}; //condensed for use in dmg roll on crit

    //Dialog for additional modifiers and actual attack
    const strDialog = new Dialog({
        title: "Melee Attack Modifiers",
        content: meleeContent,
        buttons: {
            button1: {
                label: "Disadvantage",
                callback: (html) => strDisad(html)
            },
            button2: {
                label: "Normal Attack",
                callback: (html) => strNorm(html)
            },
            button3: {
                label: "Advantage",
                callback: (html) => strAdv(html)
            }
        },
        default: "button2"
    }).render(true);

    async function strDisad(html) {
        let sitMod = html.find("input#sitMod").val();
        let rollFormula = `2d20kl + ${atkBon} + ${strMod}`;
        if (sitMod != "") {
            rollFormula = `2d20kl + ${atkBon} + ${strMod} + ${sitMod}`;
        }
        let disad = await new Roll(rollFormula).evaluate();
        let dieNum = disad.result[0] + disad.result[1];
        if (dieNum == 20) {
            //Getting Max Damage for Crit
            let dmg = new Roll("@itemDmg", itemData);
            await dmg.evaluate();
            let maxDmg = (dmg.dice[0].faces * dmg.dice[0].number) + strMod;

            disad.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemNote}</i></div>
                <hr>
                <div><b style="color: green;">CRITICAL HIT</b></div>
                </br>
                <div><b>MAX DAMAGE</b> &nbsp ${maxDmg}</div>
                </br>
                <div>NAT 19 EFFECT: ${itemNat}</div>
                </html>`
            });
        } else if (dieNum == 19) {
            disad.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemNote}</i></div>
                <hr>
                <div><b style="color: green;">NATURAL 19</b></div>
                </br>
                <div>${itemNat}</div>
                </br>
                <button class="melee-damage" data-str="${strMod}" data-dmg="${itemDmg}">Roll Damage</button>
                <hr>
                </html>`
            });
        } else if (dieNum == 1) {
            disad.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemNote}</i></div>
                <hr>
                <div><b style="color: red;">FUMBLE</b></div>
                <hr>
                </html>`
            });
        } else {
            disad.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemNote}</i></div>
                <button class="melee-damage" data-str="${strMod}" data-dmg="${itemDmg}">Roll Damage</button>
                <hr>
                </html>`
            });
        }
    }

    async function strNorm(html) {
        let sitMod = html.find("input#sitMod").val();
        let rollFormula = `1d20 + ${atkBon} + ${strMod}`;
        if (sitMod != "") {
            rollFormula = `1d20 + ${atkBon} + ${strMod} + ${sitMod}`;
        }
        let norm = await new Roll(rollFormula).evaluate();
        let dieNum = norm.result[0] + norm.result[1];
        console.log(itemData);
        if (dieNum == 20) {
            //Getting Max Damage for Crit
            let dmg = new Roll("@itemDmg", itemData);
            await dmg.evaluate();
            let maxDmg = (dmg.dice[0].faces * dmg.dice[0].number) + strMod;

            norm.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemNote}</i></div>
                <hr>
                <div><b style="color: green;">CRITICAL HIT</b></div>
                </br>
                <div><b>MAX DAMAGE</b> &nbsp ${maxDmg}</div>
                </br>
                <div>NAT 19 EFFECT: ${itemNat}</div>
                </html>`
            });
        } else if (dieNum == 19) {
            norm.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemNote}</i></div>
                <hr>
                <div><b style="color: green;">NATURAL 19</b></div>
                </br>
                <div>${itemNat}</div>
                </br>
                <button class="melee-damage" data-str="${strMod}" data-dmg="${itemDmg}">Roll Damage</button>
                <hr>
                </html>`
            });
        } else if (dieNum == 1) {
            norm.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemNote}</i></div>
                <hr>
                <div><b style="color: red;">FUMBLE</b></div>
                <hr>
                </html>`
            });
        } else {
            norm.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemNote}</i></div>
                <button class="melee-damage" data-str="${strMod}" data-dmg="${itemDmg}">Roll Damage</button>
                <hr>
                </html>`
            });
        }
    }

    async function strAdv(html) {
        let sitMod = html.find("input#sitMod").val();
        let rollFormula = `2d20kh + ${atkBon} + ${strMod}`;
        if (sitMod != "") {
            rollFormula = `2d20kh + ${atkBon} + ${strMod} + ${sitMod}`;
        }
        let adv = await new Roll(rollFormula).evaluate();
        let dieNum = adv.result[0] + adv.result[1];
        if (dieNum == 20) {
            //Getting Max Damage for Crit
            let dmg = new Roll("@itemDmg", itemData);
            await dmg.evaluate();
            let maxDmg = (dmg.dice[0].faces * dmg.dice[0].number) + strMod;

            adv.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemNote}</i></div>
                <hr>
                <div><b style="color: green;">CRITICAL HIT</b></div>
                </br>
                <div><b>MAX DAMAGE</b> &nbsp ${maxDmg}</div>
                </br>
                <div>NAT 19 EFFECT: ${itemNat}</div>
                </html>`
            });
        } else if (dieNum == 19) {
            adv.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemNote}</i></div>
                <hr>
                <div><b style="color: green;">NATURAL 19</b></div>
                </br>
                <div>${itemNat}</div>
                </br>
                <button class="melee-damage" data-str="${strMod}" data-dmg="${itemDmg}">Roll Damage</button>
                <hr>
                </html>`
            });
        } else if (dieNum == 1) {
            adv.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemNote}</i></div>
                <hr>
                <div><b style="color: red;">FUMBLE</b></div>
                <hr>
                </html>`
            });
        } else {
            adv.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemNote}</i></div>
                <button class="melee-damage" data-str="${strMod}" data-dmg="${itemDmg}">Roll Damage</button>
                <hr>
                </html>`
            });
        }
    }
}

function dragonAttack (actorInfo, itemInfo) {
    let actor = actorInfo;
    let item = itemInfo;

    //Get useful stats from actor
    let dexMod = actor.system.attributes.dex.mod;
    let atkBon = actor.system.atkBonus;
    let name = actor.name;

    //Get useful stats from item
    let itemDmg = item.system.dmg;
    let itemNat = item.system.nat19;
    let itemNote = item.system.note;
    let itemData = {itemDmg, itemNat, itemNote}; //condensed for use in dmg roll on crit

    //Dialog for addtional modifiers and actual attack
    const dexDialog = new Dialog({
        title: "Melee Attack Modifiers",
        content: meleeContent,
        buttons: {
            button1: {
                label: "Disadvantage",
                callback: (html) => dexDisad(html)
            },
            button2: {
                label: "Normal Attack",
                callback: (html) => dexNorm(html)
            },
            button3: {
                label: "Advantage",
                callback: (html) => dexAdv(html)
            }
        },
        default: "button2"
    }).render(true);

    async function dexDisad(html) {
        let sitMod = html.find("input#sitMod").val();
        let rollFormula = `2d20kl + ${atkBon} + ${dexMod}`;
        if (sitMod != "") {
            rollFormula = `2d20kl + ${atkBon} + ${dexMod} + ${sitMod}`;
        }
        let disad = await new Roll(rollFormula).evaluate();
        let dieNum = disad.result[0] + disad.result[1];
        if (dieNum == 20) {
            //Getting Max Damage for Crit
            let dmg = new Roll("@itemDmg", itemData);
            await dmg.evaluate();
            let maxDmg = (dmg.dice[0].faces * dmg.dice[0].number) + dexMod;

            disad.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemNote}</i></div>
                <hr>
                <div><b style="color: green;">CRITICAL HIT</b></div>
                </br>
                <div><b>MAX DAMAGE</b> &nbsp ${maxDmg}</div>
                </br>
                <div>NAT 19 EFFECT: ${itemNat}</div>
                </html>`
            });
        } else if (dieNum == 19) {
            disad.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemNote}</i></div>
                <hr>
                <div><b style="color: green;">NATURAL 19</b></div>
                </br>
                <div>${itemNat}</div>
                </br>
                <button class="dragon-damage" data-dex="${dexMod}" data-dmg="${itemDmg}">Roll Damage</button>
                <hr>
                </html>`
            });
        } else if (dieNum == 1) {
            disad.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemNote}</i></div>
                <hr>
                <div><b style="color: red;">FUMBLE</b></div>
                <hr>
                </html>`
            });
        } else {
            disad.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemNote}</i></div>
                <button class="dragon-damage" data-dex="${dexMod}" data-dmg="${itemDmg}">Roll Damage</button>
                <hr>
                </html>`
            });
        }
    }

    async function dexNorm(html) {
        let sitMod = html.find("input#sitMod").val();
        let rollFormula = `1d20 + ${atkBon} + ${dexMod}`;
        if (sitMod != "") {
            rollFormula = `1d20 + ${atkBon} + ${dexMod} + ${sitMod}`;
        }
        let norm = await new Roll(rollFormula).evaluate();
        let dieNum = norm.result[0] + norm.result[1];
        if (dieNum == 20) {
            //Getting Max Damage for Crit
            let dmg = new Roll("@itemDmg", itemData);
            await dmg.evaluate();
            let maxDmg = (dmg.dice[0].faces * dmg.dice[0].number) + dexMod;

            norm.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemNote}</i></div>
                <hr>
                <div><b style="color: green;">CRITICAL HIT</b></div>
                </br>
                <div><b>MAX DAMAGE</b> &nbsp ${maxDmg}</div>
                </br>
                <div>NAT 19 EFFECT: ${itemNat}</div>
                </html>`
            });
        } else if (dieNum == 19) {
            norm.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemNote}</i></div>
                <hr>
                <div><b style="color: green;">NATURAL 19</b></div>
                </br>
                <div>${itemNat}</div>
                </br>
                <button class="dragon-damage" data-dex="${dexMod}" data-dmg="${itemDmg}">Roll Damage</button>
                <hr>
                </html>`
            });
        } else if (dieNum == 1) {
            norm.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemNote}</i></div>
                <hr>
                <div><b style="color: red;">FUMBLE</b></div>
                <hr>
                </html>`
            });
        } else {
            norm.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemNote}</i></div>
                <button class="dragon-damage" data-dex="${dexMod}" data-dmg="${itemDmg}">Roll Damage</button>
                <hr>
                </html>`
            });
        }
    }

    async function dexAdv(html) {
        let sitMod = html.find("input#sitMod").val();
        let rollFormula = `2d20kh + ${atkBon} + ${dexMod}`;
        if (sitMod != "") {
            rollFormula = `2d20kh + ${atkBon} + ${dexMod} + ${sitMod}`;
        }
        let adv = await new Roll(rollFormula).evaluate();
        let dieNum = adv.result[0] + adv.result[1];
        if (dieNum == 20) {
            //Getting Max Damage for Crit
            let dmg = new Roll("@itemDmg", itemData);
            await dmg.evaluate();
            let maxDmg = (dmg.dice[0].faces * dmg.dice[0].number) + dexMod;

            adv.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemNote}</i></div>
                <hr>
                <div><b style="color: green;">CRITICAL HIT</b></div>
                </br>
                <div><b>MAX DAMAGE</b> &nbsp ${maxDmg}</div>
                </br>
                <div>NAT 19 EFFECT: ${itemNat}</div>
                </html>`
            });
        } else if (dieNum == 19) {
            adv.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemNote}</i></div>
                <hr>
                <div><b style="color: green;">NATURAL 19</b></div>
                </br>
                <div>${itemNat}</div>
                </br>
                <button class="dragon-damage" data-dex="${dexMod}" data-dmg="${itemDmg}">Roll Damage</button>
                <hr>
                </html>`
            });
        } else if (dieNum == 1) {
            adv.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemNote}</i></div>
                <hr>
                <div><b style="color: red;">FUMBLE</b></div>
                <hr>
                </html>`
            });
        } else {
            adv.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                <hr>
                <i>${itemNote}</i></div>
                <button class="dragon-damage" data-dex="${dexMod}" data-dmg="${itemDmg}">Roll Damage</button>
                <hr>
                </html>`
            });
        }
    }
}

function rangedAttack (actorInfo, itemInfo) {
    let actor = actorInfo;
    let item = itemInfo;

    //Get useful stats from actor
    let percMod = actor.system.attributes.perc.mod;
    let atkBon = actor.system.atkBonus;
    let name = actor.name;

    //Get useful stats from item
    let itemDmg = item.system.dmg;
    let itemNat = item.system.nat19;
    let itemJam = item.system.jam;
    let itemNote = item.system.note;
    let itemAmmoCurrent = item.system.magCurrent;
    let itemAmmoMax = item.system.magMax;
    let itemMags = item.system.magNumber;
    let itemData = {itemDmg, itemNat, itemJam, itemAmmoCurrent, itemAmmoMax, itemMags, itemNote}; //condensed for use in dmg roll on crit

    //Dialog for additional modifiers and actual attack
    const percDialog = new Dialog({
        title: "Ranged Attack Modifiers",
        content: rangedContent,
        buttons: {
            button1: {
                label: "Suppressive",
                callback: (html) => autoSuppress(html)
            },
            button2: {
                label: "Area Dmg",
                callback: (html) => autoArea(html)
                
            },
            button3: {
                label: "Disadvantage",
                callback: (html) => percDisad(html)
            },
            button4: {
                label: "Advantage",
                callback: (html) => percAdv(html)
            },
            button5: {
                label: "Reload",
                callback: (html) => weaponReload(html)
            },
            button6: {
                label: "Normal Attack",
                callback: (html) => percNorm(html)
            }
        },
        default: "button6"
    }).render(true);
    
    async function percDisad(html) {
        const burstSpray = html.find("#burstSpray");
        const burstFocus = html.find("#burstFocus");
        let sitMod = html.find("input#sitMod").val();
        let disad;
        let fireMode;

        //Adjusting Values and Messages Based on Inputs
        if (burstSpray[0].checked) {
            if (itemAmmoCurrent < 3) {
                ChatMessage.create({
                    content:`
                        <html>
                        <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:7px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> is <b>OUT OF AMMO</b></h3>
                        <hr>
                        <button class="reload"
                        data-actorId="${actor.uuid}"
                        data-itemId="${item.id}"
                        data-ammoCurrent="${itemAmmoCurrent}"
                        data-ammoMax="${itemAmmoMax}"
                        data-mags="${itemMags}">Reload</button>
                        </div>
                        </html>`
                });
            } else {
                console.log(sitMod);
                sitMod = sitMod + 2;
                console.log(sitMod);
                fireMode = "Burst Spray";
                item.update({'system.magCurrent': item.system.magCurrent - 3});
                let rollFormula = `2d20kl + ${atkBon} + ${percMod}`;
                if (sitMod != "") {
                    rollFormula = `2d20kl + ${atkBon} + ${percMod} + ${sitMod}`;
                }
                disad = await new Roll(rollFormula).evaluate();
            }  
        } else if (burstFocus[0].checked) {
            if (itemAmmoCurrent < 3) {
                ChatMessage.create({
                    content:`
                        <html>
                        <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:7px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> is <b>OUT OF AMMO</b></h3>
                        <hr>
                        <button class="reload"
                        data-actorId="${actor.uuid}"
                        data-itemId="${item.id}"
                        data-ammoCurrent="${itemAmmoCurrent}"
                        data-ammoMax="${itemAmmoMax}"
                        data-mags="${itemMags}">Reload</button>
                        </div>
                        </html>`
                });
            } else {
                console.log(sitMod);
                sitMod = sitMod - 2;
                console.log(sitMod);
                fireMode = "Burst Focus";
                item.update({'system.magCurrent': item.system.magCurrent - 3});
                let rollFormula = `2d20kl + ${atkBon} + ${percMod}`;
                if (sitMod != "") {
                    rollFormula = `2d20kl + ${atkBon} + ${percMod} + ${sitMod}`;
                }
                disad = await new Roll(rollFormula).evaluate();
            } 
        } else {
            if (itemAmmoCurrent < 1) {
                ChatMessage.create({
                    content:`
                        <html>
                        <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:7px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> is <b>OUT OF AMMO</b></h3>
                        <hr>
                        <button class="reload"
                        data-actorId="${actor.uuid}"
                        data-itemId="${item.id}"
                        data-ammoCurrent="${itemAmmoCurrent}"
                        data-ammoMax="${itemAmmoMax}"
                        data-mags="${itemMags}">Reload</button>
                        </div>
                        </html>`
                });
            } else {
                fireMode = "Single Shot";
                item.update({'system.magCurrent': item.system.magCurrent - 1});
                let rollFormula = `2d20kl + ${atkBon} + ${percMod}`;
                if (sitMod != "") {
                    rollFormula = `2d20kl + ${atkBon} + ${percMod} + ${sitMod}`;
                }
                disad = await new Roll(rollFormula).evaluate();
            }
        }

        let dieNum;
        if (disad) {
            dieNum = disad.result[0] + disad.result[1];
            if (dieNum == 20) {
                //Getting Max Damage for Crit
                let dmg = new Roll("@itemDmg", itemData);
                await dmg.evaluate();
                let maxDmg = (dmg.dice[0].faces * dmg.dice[0].number) + percMod;
    
                disad.toMessage({
                    speaker: ChatMessage.getSpeaker(),
                    flavor: `
                    <html>
                    <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                    <hr>
                    <i>${itemNote}</i></div>
                    <hr>
                    <i><b>Fire Mode: </b>${fireMode}</i>
                    </br>
                    <b style="color: green;">CRITICAL HIT</b>
                    </br>
                    <b>MAX DAMAGE</b> &nbsp ${maxDmg}
                    </html>`
                });
            } else if (dieNum == 19) {
                disad.toMessage({
                    speaker: ChatMessage.getSpeaker(),
                    flavor: `
                    <html>
                    <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                    <hr>
                    <i>${itemNote}</i></div>
                    <hr>
                    <i><b>Fire Mode: </b>${fireMode}</i>
                    </br>
                    <b style="color: green;">NATURAL 19</b>
                    </br>
                    ${itemNat}
                    </br>
                    <button class="perc-damage" data-perc="${percMod}" data-dmg="${itemDmg}">Roll Damage</button>
                    <hr>
                    </html>`
                });
            } else if (dieNum == 1) {
                disad.toMessage({
                    speaker: ChatMessage.getSpeaker(),
                    flavor: `
                    <html>
                    <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                    <hr>
                    <i>${itemNote}</i></div>
                    <hr>
                    <i><b>Fire Mode: </b>${fireMode}</i>
                    </br>
                    <b style="color: red;">FUMBLE</b>
                    <hr>
                    <div>Roll 1d10 to check for a jam.</div>
                    <button class="jam-check"
                        data-jam="${itemJam}"
                        data-name="${item.name}">Jam Check</button>
                    </html>`
                });
            } else {
                disad.toMessage({
                    speaker: ChatMessage.getSpeaker(),
                    flavor: `
                    <html>
                    <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                    <hr>
                    <i>${itemNote}</i></div>
                    <i><b>Fire Mode: </b>${fireMode}</i>
                    </br>
                    <button class="perc-damage" data-perc="${percMod}" data-dmg="${itemDmg}">Roll Damage</button>
                    <hr>
                    </html>`
                });
            }
        }
    }

    async function percNorm(html) {
        const burstSpray = html.find("#burstSpray");
        const burstFocus = html.find("#burstFocus");
        let sitMod = html.find("input#sitMod").val();
        let norm;
        let fireMode;

        //Adjusting Values and Messages Based on Inputs
        if (burstSpray[0].checked) {
            if (itemAmmoCurrent < 3) {
                ChatMessage.create({
                    content:`
                        <html>
                        <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:7px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> is <b>OUT OF AMMO</b></h3>
                        <hr>
                        <button class="reload"
                        data-actorId="${actor.uuid}"
                        data-itemId="${item.id}"
                        data-ammoCurrent="${itemAmmoCurrent}"
                        data-ammoMax="${itemAmmoMax}"
                        data-mags="${itemMags}">Reload</button>
                        </div>
                        </html>`
                });
            } else {
                console.log(sitMod);
                sitMod = sitMod + 2;
                console.log(sitMod);
                fireMode = "Burst Spray";
                item.update({'system.magCurrent': item.system.magCurrent - 3});
                let rollFormula = `1d20 + ${atkBon} + ${percMod}`;
                if (sitMod != "") {
                    rollFormula = `1d20 + ${atkBon} + ${percMod} + ${sitMod}`;
                }
                norm = await new Roll(rollFormula).evaluate();
            }  
        } else if (burstFocus[0].checked) {
            if (itemAmmoCurrent < 3) {
                ChatMessage.create({
                    content:`
                        <html>
                        <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:7px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> is <b>OUT OF AMMO</b></h3>
                        <hr>
                        <button class="reload"
                        data-actorId="${actor.uuid}"
                        data-itemId="${item.id}"
                        data-ammoCurrent="${itemAmmoCurrent}"
                        data-ammoMax="${itemAmmoMax}"
                        data-mags="${itemMags}">Reload</button>
                        </div>
                        </html>`
                });
            } else {
                console.log(sitMod);
                sitMod = sitMod - 2;
                console.log(sitMod);
                fireMode = "Burst Focus";
                item.update({'system.magCurrent': item.system.magCurrent - 3});
                let rollFormula = `1d20 + ${atkBon} + ${percMod}`;
                if (sitMod != "") {
                    rollFormula = `1d20 + ${atkBon} + ${percMod} + ${sitMod}`;
                }
                norm = await new Roll(rollFormula).evaluate();
            }
        } else {
            if (itemAmmoCurrent < 1) {
                ChatMessage.create({
                    content:`
                        <html>
                        <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:7px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> is <b>OUT OF AMMO</b></h3>
                        <hr>
                        <button class="reload"
                        data-actorId="${actor.uuid}"
                        data-itemId="${item.id}"
                        data-ammoCurrent="${itemAmmoCurrent}"
                        data-ammoMax="${itemAmmoMax}"
                        data-mags="${itemMags}">Reload</button>
                        </div>
                        </html>`
                });
            } else {
                fireMode = "Single Shot";
                item.update({'system.magCurrent': item.system.magCurrent - 1});
                let rollFormula = `1d20 + ${atkBon} + ${percMod}`;
                if (sitMod != "") {
                    rollFormula = `1d20 + ${atkBon} + ${percMod} + ${sitMod}`;
                }
                norm = await new Roll(rollFormula).evaluate();
            }      
        }

        let dieNum;
        if (norm) {
            dieNum = norm.result[0] + norm.result[1];
            if (dieNum == 20) {
                //Getting Max Damage for Crit
                let dmg = new Roll("@itemDmg", itemData);
                await dmg.evaluate();
                let maxDmg = (dmg.dice[0].faces * dmg.dice[0].number) + percMod;
    
                norm.toMessage({
                    speaker: ChatMessage.getSpeaker(),
                    flavor: `
                    <html>
                    <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                    <hr>
                    <i>${itemNote}</i></div>
                    <hr>
                    <i><b>Fire Mode: </b>${fireMode}</i>
                    </br>
                    <b style="color: green;">CRITICAL HIT</b>
                    </br>
                    <b>MAX DAMAGE</b> &nbsp ${maxDmg}
                    </html>`
                });
            } else if (dieNum == 19) {
                norm.toMessage({
                    speaker: ChatMessage.getSpeaker(),
                    flavor: `
                    <html>
                    <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                    <hr>
                    <i>${itemNote}</i></div>
                    <hr>
                    <i><b>Fire Mode: </b>${fireMode}</i>
                    </br>
                    <div><b style="color: green;">NATURAL 19</b></div>
                    </br>
                    ${itemNat}
                    </br>
                    <button class="perc-damage" data-perc="${percMod}" data-dmg="${itemDmg}">Roll Damage</button>
                    <hr>
                    </html>`
                });
            } else if (dieNum == 1) {
                norm.toMessage({
                    speaker: ChatMessage.getSpeaker(),
                    flavor: `
                    <html>
                    <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                    <hr>
                    <i>${itemNote}</i></div>
                    <hr>
                    <i><b>Fire Mode: </b>${fireMode}</i>
                    </br>
                    <div><b style="color: red;">FUMBLE</b></div>
                    <hr>
                    <div>Roll 1d10 to check for a jam.</div>
                    <button class="jam-check"
                        data-jam="${itemJam}"
                        data-name="${item.name}">Jam Check</button>
                    </html>`
                });
            } else {
                norm.toMessage({
                    speaker: ChatMessage.getSpeaker(),
                    flavor: `
                    <html>
                    <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                    <hr>
                    <i>${itemNote}</i></div>
                    <i><b>Fire Mode: </b>${fireMode}</i>
                    </br>
                    <button class="perc-damage" data-perc="${percMod}" data-dmg="${itemDmg}">Roll Damage</button>
                    <hr>
                    </html>`
                });
            }
        }
    }

    async function percAdv(html) {
        const burstSpray = html.find("#burstSpray");
        const burstFocus = html.find("#burstFocus");
        let sitMod = html.find("input#sitMod").val();
        let adv;
        let fireMode;

        //Adjusting Values and Messages Based on Inputs
        if (burstSpray[0].checked) {
            if (itemAmmoCurrent < 3) {
                ChatMessage.create({
                    content:`
                        <html>
                        <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:7px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> is <b>OUT OF AMMO</b></h3>
                        <hr>
                        <button class="reload"
                        data-actorId="${actor.uuid}"
                        data-itemId="${item.id}"
                        data-ammoCurrent="${itemAmmoCurrent}"
                        data-ammoMax="${itemAmmoMax}"
                        data-mags="${itemMags}">Reload</button>
                        </div>
                        </html>`
                });
            } else {
                console.log(sitMod);
                sitMod = sitMod + 2;
                console.log(sitMod);
                fireMode = "Burst Spray";
                item.update({'system.magCurrent': item.system.magCurrent - 3});
                let rollFormula = `2d20kh + ${atkBon} + ${percMod}`;
                if (sitMod != "") {
                    rollFormula = `2d20kh + ${atkBon} + ${percMod} + ${sitMod}`;
                }
                adv = await new Roll(rollFormula).evaluate();
            }    
        } else if (burstFocus[0].checked) {
            if (itemAmmoCurrent < 3) {
                ChatMessage.create({
                    content:`
                        <html>
                        <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:7px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> is <b>OUT OF AMMO</b></h3>
                        <hr>
                        <button class="reload"
                        data-actorId="${actor.uuid}"
                        data-itemId="${item.id}"
                        data-ammoCurrent="${itemAmmoCurrent}"
                        data-ammoMax="${itemAmmoMax}"
                        data-mags="${itemMags}">Reload</button>
                        </div>
                        </html>`
                });
            } else {
                console.log(sitMod);
                sitMod = sitMod - 2;
                console.log(sitMod);
                fireMode = "Burst Focus";
                item.update({'system.magCurrent': item.system.magCurrent - 3});
                let rollFormula = `2d20kh + ${atkBon} + ${percMod}`;
                if (sitMod != "") {
                    rollFormula = `2d20kh + ${atkBon} + ${percMod} + ${sitMod}`;
                }
                adv = await new Roll(rollFormula).evaluate();
            }  
        } else {
            if (itemAmmoCurrent < 1) {
                ChatMessage.create({
                    content:`
                        <html>
                        <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:7px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> is <b>OUT OF AMMO</b></h3>
                        <hr>
                        <button class="reload"
                        data-actorId="${actor.uuid}"
                        data-itemId="${item.id}"
                        data-ammoCurrent="${itemAmmoCurrent}"
                        data-ammoMax="${itemAmmoMax}"
                        data-mags="${itemMags}">Reload</button>
                        </div>
                        </html>`
                });
            } else {
                fireMode = "Single Shot";
                item.update({'system.magCurrent': item.system.magCurrent - 1});
                let rollFormula = `2d20kh + ${atkBon} + ${percMod}`;
                if (sitMod != "") {
                    rollFormula = `2d20kh + ${atkBon} + ${percMod} + ${sitMod}`;
                }
                adv = await new Roll(rollFormula).evaluate();
            }
        }

        let dieNum;
        if (adv) {
            dieNum = adv.result[0] + adv.result[1];
            if (dieNum == 20) {
                //Getting Max Damage for Crit
                let dmg = new Roll("@itemDmg", itemData);
                await dmg.evaluate();
                let maxDmg = (dmg.dice[0].faces * dmg.dice[0].number) + percMod;
    
                adv.toMessage({
                    speaker: ChatMessage.getSpeaker(),
                    flavor: `
                    <html>
                    <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                    <hr>
                    <i>${itemNote}</i></div>
                    <hr>
                    <i><b>Fire Mode: </b>${fireMode}</i>
                    </br>
                    <b style="color: green;">CRITICAL HIT</b>
                    </br>
                    <b>MAX DAMAGE</b> &nbsp ${maxDmg}
                    </br>
                    </html>`
                });
            } else if (dieNum == 19) {
                adv.toMessage({
                    speaker: ChatMessage.getSpeaker(),
                    flavor: `
                    <html>
                    <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                    <hr>
                    <i>${itemNote}</i></div>
                    <hr>
                    <i><b>Fire Mode: </b>${fireMode}</i>
                    </br>
                    <b style="color: green;">NATURAL 19</b>
                    </br>
                    ${itemNat}
                    </br>
                    <button class="perc-damage" data-perc="${percMod}" data-dmg="${itemDmg}">Roll Damage</button>
                    <hr>
                    </html>`
                });
            } else if (dieNum == 1) {
                adv.toMessage({
                    speaker: ChatMessage.getSpeaker(),
                    flavor: `
                    <html>
                    <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                    <hr>
                    <i>${itemNote}</i></div>
                    <hr>
                    <i><b>Fire Mode: </b>${fireMode}</i>
                    </br>
                    <div><b style="color: red;">FUMBLE</b></div>
                    <hr>
                    <div>Roll 1d10 to check for a jam.</div>
                    <button class="jam-check"
                        data-jam="${itemJam}"
                        data-name="${item.name}">Jam Check</button>
                    </html>`
                });
            } else {
                adv.toMessage({
                    speaker: ChatMessage.getSpeaker(),
                    flavor: `
                    <html>
                    <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> attacks with their <b>${item.name}</b></h3>
                    <hr>
                    <i>${itemNote}</i></div>
                    <i><b>Fire Mode: </b>${fireMode}</i>
                    </br>
                    <button class="perc-damage" data-perc="${percMod}" data-dmg="${itemDmg}">Roll Damage</button>
                    <hr>
                    </html>`
                });
            }
        } 
    }

    function autoSuppress(html) {
        let autoExtra = parseInt(html.find("input#autoExtraBullets").val());
        let bulletsShot;
        if (autoExtra) {
            bulletsShot = autoExtra + 10;
        } else {
            bulletsShot = 10;
        }
        let fireMode = "Full Auto Suppressive Fire";
        let frontage = Math.floor(bulletsShot / 2);

        if (itemAmmoCurrent < bulletsShot) {
            ChatMessage.create({
                content:`
                    <html>
                    <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:7px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> is <b>OUT OF AMMO</b></h3>
                    <hr>
                    <button class="reload"
                        data-actorId="${actor.uuid}"
                        data-itemId="${item.id}"
                        data-ammoCurrent="${itemAmmoCurrent}"
                        data-ammoMax="${itemAmmoMax}"
                        data-mags="${itemMags}">Reload</button>
                    </div>
                    </html>`
            });
        } else {
            item.update({'system.magCurrent': item.system.magCurrent - bulletsShot});
            ChatMessage.create({
                content:`
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:7px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> is laying down <b>SUPPRESSIVE FIRE</b></h3>
                <hr style="margin-bottom: 10px;">
                <i><b>Fire Mode: </b>${fireMode}</i><br>
                <i><b>Bullets Fired: </b>${bulletsShot}</i><br>
                <i><b>Frontage: </b>${frontage}m wide</i><br>
                <hr>
                <i>Targets in the area (including PCs) must make a WILL check to do anything other than take cover on their next turn.
                Suppressed targets immediately move to the nearest cover, if possible, and may use their next action to do things that won't expose them to direct fire.</i></div>
                </html>`
            });
        }
    }

    function autoArea(html) {
        let autoExtra = parseInt(html.find("input#autoExtraBullets").val());
        let bulletsShot;
        if (autoExtra) {
            bulletsShot = autoExtra + 10;
        } else {
            bulletsShot = 10;
        }
        let fireMode = "Full Auto Area Damage";
        let frontage = Math.floor(bulletsShot / 2);

        if (itemAmmoCurrent < bulletsShot) {
            ChatMessage.create({
                content:`
                    <html>
                    <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:7px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> is <b>OUT OF AMMO</b></h3>
                    <hr>
                    <button class="reload"
                        data-actorId="${actor.uuid}"
                        data-itemId="${item.id}"
                        data-ammoCurrent="${itemAmmoCurrent}"
                        data-ammoMax="${itemAmmoMax}"
                        data-mags="${itemMags}">Reload</button>
                    </div>
                    </html>`
            });
        } else {
            item.update({'system.magCurrent': item.system.magCurrent - bulletsShot});
            ChatMessage.create({
                content:`
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:7px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> is firing for <b>AREA DAMAGE</b></h3>
                <hr style="margin-bottom: 10px;">
                <i><b>Fire Mode: </b>${fireMode}</i><br>
                <i><b>Bullets Fired: </b>${bulletsShot}</i><br>
                <i><b>Frontage: </b>${frontage}m wide</i><br>
                <hr>
                <i>Targets in the zone must make a Luck (DEX) save to avoid a standard hit. Targets that succeed immediately move to the nearest cover.
                The number of targets is limited to half the number of bullets fired.</i></div>
                </br>
                <button class="perc-damage" data-perc="${percMod}" data-dmg="${itemDmg}">Roll Damage</button>
                <hr>
                </html>`
            });
        }
    }

    function weaponReload(html) {
        if (itemMags > 0) {
            item.update({'system.magCurrent': itemAmmoMax});
            item.update({'system.magNumber': item.system.magNumber - 1});
            ChatMessage.create({
                content:`
                    <html>
                    <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:7px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> reloaded their ${item.name}.</b></h3>
                    <hr>
                    </div>
                    </html>`
            });
        } else {
            ChatMessage.create({
                content:`
                    <html>
                    <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:7px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${name}</b> is <b>OUT OF MAGAZINES.</b></h3>
                    <hr>
                    </div>
                    </html>`
            });
        }
    }
}

export { meleeAttack, dragonAttack, rangedAttack }