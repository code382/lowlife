export const lowlife = {};

lowlife.attackTypes = {
    none: "",
    perc: "perc",
    str: "str",
    dex: "dex"
}

lowlife.weaponTypes = {
    gun: "Gun",
    melee: "Melee",
    projectile: "Projectile",
    magicWeapon: "Magic Weapon"
}

lowlife.supplyTypes = {
    legal: "L",
    controlled: "C",
    prohibited: "P",
    military: "M"
}

lowlife.ammoTracking = {
    current: "Current Mag",
    number: "Number of Mags",
    type: "Type"
}

lowlife.ammoTypes = {
    caseless: "Caseless",
    shrapnel: "Shrapnel",
    nonlethal: "Non Lethal",
    armourPiercing: "Armour Piercing",
    specialMetals: "Special Metals"
}

lowlife.vehicleTypes = {
    car: "Car",
    truck: "Truck",
    bike: "Bike",
    aircraft: "Aircraft",
    boat: "Boat",
    drone: "Drone"
}
    
lowlife.vehicleSpeeds = {
    vSlow: "Very Slow",
    slow: "Slow",
    mod: "Moderate",
    fast: "Fast",
    vFast: "Very Fast",
    extreme: "Extreme"
}

lowlife.spellDurations = {
    none: "",
    spec: "Special",
    instant: "Instant",
    second: "Seconds",
    round: "Rounds",
    minute: "Minutes",
    day: "Days"
}

lowlife.drugTypes = {
    combat: "Combat",
    utility: "Utility",
    recreation: "Recreation"
}

lowlife.drugOnset = {
    none: "",
    immed: "Immediate",
    round: "Rounds",
    minute: "Minutes"
}

lowlife.drugDurations = {
    none: "",
    spec: "Special",
    round: "Rounds",
    minute: "Minutes",
    hour: "Hours"
}

lowlife.toxinVectors = {
    none: "",
    blood: "Blood",
    contact: "Contact",
    inhale: "Inhalation"
}

lowlife.toxinOnset = {
    none: "",
    immed: "Immediate",
    round: "Rounds"
}

lowlife.toxinDurations = {
    none: "",
    spec: "Special",
    perm: "Permanent",
    minute: "Minutes",
    hour: "Hours",
    day: "Days"
}

lowlife.gearTypes = {
    none: "",
    med: "Med",
    vis: "Vision",
    aud: "Audio",
    com: "Comms",
    util: "Utility",
    cyber: "Cyberware",
    misc: "Misc."
}

lowlife.setbackTypes = {
    none: "",
    inj: "Injury",
    mad: "Madness",
    trauma: "Trauma"
}

lowlife.attributes = {
    none: "",
    str: "STR",
    dex: "DEX",
    con: "CON",
    int: "INT",
    perc: "PERC",
    will: "WILL",
    cha: "CHA"
}

lowlife.livingStandard = {
    destitute: "Destitute",
    low: "Low",
    moderate: "Moderate",
    high: "High",
    filthyRich: "Filthy Rich"
}