async function lowlifeRoll({ value = 10, label = '', mod = 0, dice = '1d20', adv = false, disad = false }) {
    const target = parseInt(value, 10) + mod;
    let { gs, tf } = getGsTf(target);

    if (adv) {
        dice = '2d20kl';
    }

    if (disad) {
        dice = '2d20kh';
    }
    
    let rollFormula = dice;
    const roll = await new Roll(rollFormula).evaluate();
    return { roll, target, gs, tf, label }
}

function successLevelMessage(result) {
    let message = `${result.label.toUpperCase()} Target: ${result.target}`
    if (result.gs && result.tf) {
        message = `${message} GS: ${result.gs} TF: ${result.tf}`
    }
    if (result.roll.total >= result.tf)
        return `<span style="color:red; font-weight:bold;">Terrible Failure</span> (${message})`;
    if (result.roll.total > result.target)
        return `<span style="color:red; font-weight:bold;">Failure</span> (${message})`;
    if (result.roll.total <= result.gs)
        return `<span style="color:green; font-weight:bold;">Great Success</span> (${message})`;
    return `<span style="color:green; font-weight:bold;">Success</span> (${message})`;
}

function getGsTf(value) {
    let gs = 0;
    let tf = 0;
    if (value >= 13) {
        gs = Math.floor(value / 2);
        tf = 20;
    } else {
        gs = Math.floor(value / 2);
        tf = Math.floor(value * 1.5);
    }

    return { gs, tf }
}

export { lowlifeRoll, getGsTf, successLevelMessage }