let meleeContent = `
    <html>
    <table class="section-table">
    <tr>
        <td><label for="sitMod">Situational Modifier</label></td>
        <td><input type="text" id="sitMod" placeholder="e.g.: -1 or +2" value="0"></td>
    </tr>
    </table>
    </html>`
    ;

let rangedContent = `
    <html>

    <style>
    button {
        height: 34px;
    }
    </style>

    <table class="section-table">
    <tr>
        <th colspan="2"><label for="sitMod">Situational Modifier</label></th>
    </tr>
    <tr>
        <td colspan="2"><input type="text" id="sitMod" placeholder="e.g.: -1 or +2"></td>
    </tr>
    <tr>
        <th colspan="2">BURST FIRE</th>
    </tr>
    <tr>
        <td style="width: 50%;"><input type="checkbox" id="burstSpray"><label for="burstSpray">Spray</label></td>
        <td style="width: 50%;"><input type="checkbox" id="burstFocus"><label for="burstFocus">Focus</label></td>
    </tr>
    <tr>
        <th colspan="2"><label for="autoExtraBullets">Full Auto Extra Bullets</label></th>
    </tr>
    <tr>
    <td style="width: 50%; padding: 1px 3px 1px 3px;">Number of bullets fired over the default of 10 for Full Auto (Area Dmg or Suppressive)</td>
        <td style="width: 50%;"><input type="text" id="autoExtraBullets" placeholder="only positive numbers"></td>
    </tr>
    </table>
    </html>`

let spellContent = `<html>
    <table class="section-table">
    <tr>
        <td><label for="sitMod">Situational Modifier</label></td>
        <td><input type="text" id="sitMod" placeholder="e.g.: -1 or +2" value="0"></td>
    </tr>
    <tr>
        <td colspan="2">Casters take a -2 penalty for each pice of cyberware they have (not currently added to the check, so enter penalty above). Mages and Exarchs may access their reroll pool.</td>
    </tr>
    </table>
    </html>`;

export { meleeContent, rangedContent, spellContent }