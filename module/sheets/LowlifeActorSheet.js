import { lowlifeRoll, successLevelMessage } from "../lowlifeutils.js";
import { meleeAttack, dragonAttack, rangedAttack } from "../dice.js";

export default class LowlifeActorSheet extends ActorSheet {
    static get defaultOptions () {
        return foundry.utils.mergeObject(super.defaultOptions, {
            width: 815,
            classes: ["lowlife", "sheet", "actor"],
            tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-content", initial: "combat" }]
        });
    }

    get template() {
        return `systems/lowlife/templates/sheets/actor/${this.actor.type}-sheet.html`;
    }

    getData() {
        const data = super.getData();
        data.config = CONFIG.lowlife;
        data.dtypes = ["String", "Number", "Boolean"];
        data.weapon = data.items.filter(function(item) {return item.type == "weapon"});
        data.specialAmmo = data.items.filter(function(item) {return item.type == "specialAmmo"});
        data.vehicle = data.items.filter(function(item) {return item.type == "vehicle"});
        data.spell = data.items.filter(function(item) {return item.type == "spell"});
        data.armour = data.items.filter(function(item) {return item.type == "armour"});
        data.drug = data.items.filter(function(item) {return item.type == "drug"});
        data.toxin = data.items.filter(function(item) {return item.type == "toxin"});
        data.gear = data.items.filter(function(item) {return item.type == "gear"});
        data.contact = data.items.filter(function(item) {return item.type == "contact"});
        data.ability = data.items.filter(function(item) {return item.type == "ability"});
        data.setback = data.items.filter(function(item) {return item.type == "setback"});
        return data;
    }

    activateListeners(html) {
        html.find(".item-create").click(this._onItemCreate.bind(this));
        html.find(".item-edit").click(this._onItemEdit.bind(this));
        html.find(".item-delete").click(this._onItemDelete.bind(this));
        html.find(".attr-roll").click(this._onAttrRoll.bind(this));
        html.find(".luck-roll").click(this._onLuckRoll.bind(this));
        html.find(".initiative-roll").click(this._onInitiativeRoll.bind(this));
        html.find("#short-rest").click(this._onShortRest.bind(this));
        html.find("#long-rest").click(this._onLongRest.bind(this));
        html.find(".item-roll").click(this._onItemRoll.bind(this));
        html.find(".weapon-roll").click(this._onWeaponRoll.bind(this));
        html.find(".specialty-roll").click(this._onSpecialtyRoll.bind(this));
        html.find(".enemy-weapon1").click(this._onEnemyWeapon1.bind(this));
        html.find(".enemy-weapon2").click(this._onEnemyWeapon2.bind(this));
        html.find(".enemy-weapon3").click(this._onEnemyWeapon3.bind(this));

        super.activateListeners(html);
    }

    //Handle Owned Items
    _onItemCreate(event) {
        event.preventDefault();
        let element = event.currentTarget;
        
        let itemData = {
            name: "New Item",
            type: element.dataset.type
        };

        return this.actor.createEmbeddedDocuments("Item", [itemData]);
    }

    _onItemEdit(event) {
        event.preventDefault();
        let element = event.currentTarget;
        let itemId = element.closest(".item").dataset.itemId;
        let item = this.actor.items.get(itemId);

        item.sheet.render(true);
    }

    _onItemDelete(event) {
        event.preventDefault();
        let element = event.currentTarget;
        let itemId = element.closest(".item").dataset.itemId;
        return this.actor.deleteEmbeddedDocuments("Item", [itemId]);
    }


    //Roll Functions and Stats
    async roll(data) {
        const result = await lowlifeRoll(data);
        result.roll.toMessage({
            speaker: ChatMessage.getSpeaker({ actor: this.actor }),
            flavor: successLevelMessage(result) });
        return result;
    }

    getLuckModifier(){
        const modifier = $('#luck-modifier')[0].value;
        if (!modifier){
            return 0;
        }
        return this.actor.system.attributes[modifier].mod;
    }

    async decrementLuck(){
        if (this.actor.system.luck.value <= 0){
            return false;
        }
        try {
            await this.actor.update({[`system.luck.value`]: this.actor.system.luck.value - 1});
        } catch(e){
            console.log(e)
        }
    }


    //Rolling Attribute Checks
    _onAttrRoll(event) {
        event.preventDefault();
        let clicked = this;

        //add dialog for modifiers, roll from dialog
        const modDialog = new Dialog({
        title: "Situational Modifiers",
        content: `<html>
            <table class="section-table">
            <tr>
            <td><label for="sitMod">Situational Modifier</label></td>
            <td><input class="sitMod" type="text" id="sitMod" placeholder="e.g.: 1 or -2"></td>
            </tr>
            </table>
            </html>`,
        buttons: {
            button1: {
            label: "Disadvantage",
            callback: (html) => disadRoll(html)
            },
            button2: {
            label: "Normal Roll",
            callback: (html) => normalRoll(html)
            },
            button3: {
            label: "Advantage",
            callback: (html) => advRoll(html)
            }
        },
        default: "button2"
        }).render(true);

        async function disadRoll(html) {
        let sitMod = html.find("input#sitMod").val();
        let disad = true;
        const result = await clicked.roll({
            ...event.currentTarget.dataset,
            disad,
            sitMod
        });
        console.log(result);
        }

        async function normalRoll(html) {
        let sitMod = html.find("input#sitMod").val();
        const result = await clicked.roll({
            ...event.currentTarget.dataset,
            sitMod
        });
        console.log(result);
        }

        async function advRoll(html) {
        let sitMod = html.find("input#sitMod").val();
        let adv = true;
        const result = await clicked.roll({
            ...event.currentTarget.dataset,
            adv,
            sitMod
        });
        console.log(result);
        }
    }

    //Rolling Luck Checks
    async _onLuckRoll(event) {
        event.preventDefault();
        let mod = this.getLuckModifier();
        let clicked = this;

        //add dialog for modifiers, roll from dialog
        const modDialog = new Dialog({
            title: "Situational Modifiers",
            content: `<html>
                <table class="section-table">
                <tr>
                <td><label for="sitMod">Situational Modifier</label></td>
                <td><input class="sitMod" type="text" id="sitMod" placeholder="e.g.: 1 or -2"></td>
                </tr>
                </table>
                </html>`,
            buttons: {
                button1: {
                label: "Disadvantage",
                callback: (html) => disadRoll(html)
                },
                button2: {
                label: "Normal Roll",
                callback: (html) => normalRoll(html)
                },
                button3: {
                label: "Advantage",
                callback: (html) => advRoll(html)
                }
            },
            default: "button2"
        }).render(true);

        async function disadRoll(html) {
            let sitMod = html.find("input#sitMod").val();
            let disad = true;
            const result = await clicked.roll({
                ...event.currentTarget.dataset,
                mod: mod,
                disad,
                sitMod
            });
            console.log(result);
            if (result.roll.total <= result.target) {
                clicked.decrementLuck();
            }
        }

        async function normalRoll(html) {
            let sitMod = html.find("input#sitMod").val();
            const result = await clicked.roll({
                ...event.currentTarget.dataset,
                mod: mod,
                sitMod
            });
            console.log(result);
            if (result.roll.total <= result.target) {
                clicked.decrementLuck();
            }
        }

        async function advRoll(html) {
            let sitMod = html.find("input#sitMod").val();
            let adv = true;
            const result = await clicked.roll({
                ...event.currentTarget.dataset,
                mod: mod,
                adv,
                sitMod
            });
            console.log(result);
            if (result.roll.total <= result.target) {
                clicked.decrementLuck();
            }
        }
    }

    //Rolling Initiative Checks
    _onInitiativeRoll(event) {
        event.preventDefault();
        this.roll({...event.currentTarget.dataset,
            adv: event.shiftKey,
            disad: event.altKey
        });
    }

    //Rest Automation
    async _onShortRest(event) {
        event.preventDefault();
        console.log('SHORT REST');

        //get current Will value for comparing rolls
        let actor = this.actor;
        let will = actor.system.attributes.will.value;

        //roll the checks
        let checks = await new Roll(`2d20cs<=${will}`).evaluate({async: true});

        let flavorMessage = `<html>
                                <img style="width:50px; height: 50px; float: left; margin: 3px 5px 0 0;" src="${actor.img}">
                                ${actor.name} takes some time recovering from their injuries. <hr>
                                They may gain <b>${checks.total}</b> of the following benefits:<br>
                                <ul>
                                    <li>Recover one use of an expended class ability.</li>
                                    <li>Recover one Reroll Pool die.</li>
                                    <li>Recover one point of Attribute loss, excluding Luck</li>
                                </ul>
                            </html>`
        if (checks.total < 1) {
            flavorMessage = `<html>
                                <img style="width:50px; height: 50px; float: left; margin: 3px 5px 0 0;" src="${actor.img}">
                                ${actor.name} takes some time recovering from their injuries. <hr>
                                They are unable to recover anything during this Short Rest.
                            </html>`
        }
        checks.toMessage({
            speaker: ChatMessage.getSpeaker(),
            flavor: flavorMessage
        });
    }

    async _onLongRest(event) {
        event.preventDefault();

        console.log('LONG REST');

        //get current Will value for comparing rolls
        let actor = this.actor;
        let will = actor.system.attributes.will.value;

        //update the hp and check for Fatigued condition
        if (actor.system.hp.value < actor.system.hp.max) {
            actor.update({"system.hp.value": actor.system.hp.value + 1});
        }
        if (actor.system.conditions.fatigued == true) {
            actor.update({"system.conditions.fatigued": false});
        }

        //roll the checks
        let checks = await new Roll(`2d20cs<=${will}`).evaluate({async: true});

        let flavorMessage = `<html>
                                <img style="width:50px; height: 50px; float: left; margin: 3px 5px 0 0;" src="${actor.img}">
                                ${actor.name} takes a Rong Rest. <hr>
                                They have regained <b>1HP</b> and the Fatigued condition has been removed/avoided.<hr>
                                Additionally, they may gain <b>${checks.total}</b> of the following Short Rest benefits:<br>
                                <ul>
                                    <li>Recover one use of an expended class ability.</li>
                                    <li>Recover one Reroll Pool die.</li>
                                    <li>Recover one point of Attribute loss, excluding Luck</li>
                                </ul>
                            </html>`
        if (checks.total < 1) {
            flavorMessage = `<html>
                                <img style="width:50px; height: 50px; float: left; margin: 3px 5px 0 0;" src="${actor.img}">
                                ${actor.name} takes a Rong Rest. <hr>
                                They have regained <b>1HP</b> and the Fatigued condition has been removed/avoided.<hr>
                                They were not able to gain any Short Rest benefits.
                            </html>`
        }
        checks.toMessage({
            speaker: ChatMessage.getSpeaker(),
            flavor: flavorMessage
        });
    }

    //Rolling Items
    _onItemRoll(event) {
        const itemID = event.currentTarget.closest(".item").dataset.itemId;
        const item = this.actor.items.get(itemID);

        item.rollItem();
    }

    //Rolling Weapons
    _onWeaponRoll(event) {
        event.preventDefault();

        //Get actor and item for functions
        let actor = this.actor;
        let itemID = event.currentTarget.closest(".item").dataset.itemId;
        let item = this.actor.items.get(itemID);
        console.log(actor);
        console.log(item);

        //Switch case for choosing function
        switch (item.system.attackWith) {
            case "str":
                meleeAttack(actor, item);
                break;
            case "dex":
                dragonAttack(actor, item);
                break;
            case "perc":
                rangedAttack(actor, item);
                break;
            default:
                ui.notifications.warn("Select an Attack Stat for your Weapon");
        }
    }

    //Rolling Specialty Ammo
    _onSpecialtyRoll(event) {
        event.preventDefault();

        //Get actor and item for functions
        let actor = this.actor;
        let itemID = event.currentTarget.closest(".item").dataset.itemId;
        let item = this.actor.items.get(itemID);
        console.log(actor);
        console.log(item);

        rangedAttack(actor, item);
    }

    //Rolling Enemy Attacks
    async _onEnemyWeapon1(event) {
        event.preventDefault();

        const enemyWeaponOne = this.actor.system.attacks.one.name;
        const enemyDamageOne = this.actor.system.attacks.one.dmg;
        const enemyAtkBon = this.actor.system.atkBonus;
        const enemyNat = this.actor.system.nat19;

        let rollData = {enemyWeaponOne, enemyDamageOne, enemyAtkBon};

        let attackOne = await new Roll("1d20 + @enemyAtkBon", rollData).evaluate({async: true});
        let dieNum = attackOne.result[0] + attackOne.result[1];

        if (dieNum == "20") {
            let dmg = await new Roll("@enemyDamageOne", rollData).evaluate({async: true});
            let maxDmg = dmg.dice[0].faces * dmg.dice[0].number;
            attackOne.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${this.actor.img}"> <h3 style="border-bottom: none;"><b>${this.actor.name}</b> attacks with their <b>${enemyWeaponOne}</b></h3>
                </div>
                <hr>
                <div><b style="color: green;">CRITICAL HIT</b></div>
                </br>
                <div><b>MAX DAMAGE</b> &nbsp ${maxDmg}</div>
                </html>`
            });
        } else if (dieNum == "19") {
            attackOne.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${this.actor.img}"> <h3 style="border-bottom: none;"><b>${this.actor.name}</b> attacks with their <b>${enemyWeaponOne}</b></h3>
                </div>
                <hr>
                <div><b style="color: green;">NATURAL 19</b></div>
                </br>
                <div><b>Natural 19</b> &nbsp ${enemyNat}</div>
                <button class="enemy-damage1" data-dmg="${enemyDamageOne}">Roll Damage</button>
                </html>`
            });
        } else if (dieNum == 1) {
            attackOne.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${this.actor.img}"> <h3 style="border-bottom: none;"><b>${this.actor.name}</b> attacks with their <b>${enemyWeaponOne}</b></h3>
                </div>
                <hr>
                <div><b style="color: red;">FUMBLE</b></div>
                </html>`
            });
        } else {
            attackOne.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${this.actor.img}"> <h3 style="border-bottom: none;"><b>${this.actor.name}</b> attacks with their <b>${enemyWeaponOne}</b></h3>
                </div>
                <hr>
                <button class="enemy-damage1" data-dmg="${enemyDamageOne}">Roll Damage</button>
                </html>`
            });
        }
    }

    async _onEnemyWeapon2(event) {
        event.preventDefault();

        const enemyWeaponTwo = this.actor.system.attacks.two.name;
        const enemyDamageTwo = this.actor.system.attacks.two.dmg;
        const enemyAtkBon = this.actor.system.atkBonus;
        const enemyNat = this.actor.system.nat19;

        let rollData = {enemyWeaponTwo, enemyDamageTwo, enemyAtkBon};

        let attackTwo = await new Roll("1d20 + @enemyAtkBon", rollData).evaluate({async: true});
        let dieNum = attackTwo.result[0] + attackTwo.result[1];

        if (dieNum == "20") {
            let dmg = await new Roll("@enemyDamageTwo", rollData).evaluate({async: true});
            let maxDmg = dmg.dice[0].faces * dmg.dice[0].number;
            attackTwo.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${this.actor.img}"> <h3 style="border-bottom: none;"><b>${this.actor.name}</b> attacks with their <b>${enemyWeaponTwo}</b></h3>
                </div>
                <hr>
                <div><b style="color: green;">CRITICAL HIT</b></div>
                </br>
                <div><b>MAX DAMAGE</b> &nbsp ${maxDmg}</div>
                </html>`
            });
        } else if (dieNum == "19") {
            attackTwo.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${this.actor.img}"> <h3 style="border-bottom: none;"><b>${this.actor.name}</b> attacks with their <b>${enemyWeaponTwo}</b></h3>
                </div>
                <hr>
                <div><b style="color: green;">NATURAL 19</b></div>
                </br>
                <div><b>Natural 19</b> &nbsp ${enemyNat}</div>
                <button class="enemy-damage2" data-dmg="${enemyDamageTwo}">Roll Damage</button>
                </html>`
            });
        } else if (dieNum == 1) {
            attackTwo.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${this.actor.img}"> <h3 style="border-bottom: none;"><b>${this.actor.name}</b> attacks with their <b>${enemyWeaponTwo}</b></h3>
                </div>
                <hr>
                <div><b style="color: red;">FUMBLE</b></div>
                </html>`
            });
        } else {
            attackTwo.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${this.actor.img}"> <h3 style="border-bottom: none;"><b>${this.actor.name}</b> attacks with their <b>${enemyWeaponTwo}</b></h3>
                </div>
                <hr>
                <button class="enemy-damage2" data-dmg="${enemyDamageTwo}">Roll Damage</button>
                </html>`
            });
        }
    }

    async _onEnemyWeapon3(event) {
        event.preventDefault();

        const enemyWeaponThree = this.actor.system.attacks.three.name;
        const enemyDamageThree = this.actor.system.attacks.three.dmg;
        const enemyAtkBon = this.actor.system.atkBonus;
        const enemyNat = this.actor.system.nat19;

        let rollData = {enemyWeaponThree, enemyDamageThree, enemyAtkBon};

        let attackThree = await new Roll("1d20 + @enemyAtkBon", rollData).evaluate({async: true});
        let dieNum = attackThree.result[0] + attackThree.result[1];

        if (dieNum == "20") {
            let dmg = await new Roll("@enemyDamageThree", rollData).evaluate({async: true});
            let maxDmg = dmg.dice[0].faces * dmg.dice[0].number;
            attackThree.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${this.actor.img}"> <h3 style="border-bottom: none;"><b>${this.actor.name}</b> attacks with their <b>${enemyWeaponThree}</b></h3>
                </div>
                <hr>
                <div><b style="color: green;">CRITICAL HIT</b></div>
                </br>
                <div><b>MAX DAMAGE</b> &nbsp ${maxDmg}</div>
                </html>`
            });
        } else if (dieNum == "19") {
            attackThree.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${this.actor.img}"> <h3 style="border-bottom: none;"><b>${this.actor.name}</b> attacks with their <b>${enemyWeaponThree}</b></h3>
                </div>
                <hr>
                <div><b style="color: green;">NATURAL 19</b></div>
                </br>
                <div><b>Natural 19</b> &nbsp ${enemyNat}</div>
                <button class="enemy-damage3" data-dmg="${enemyDamageThree}">Roll Damage</button>
                </html>`
            });
        } else if (dieNum == 1) {
            attackThree.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${this.actor.img}"> <h3 style="border-bottom: none;"><b>${this.actor.name}</b> attacks with their <b>${enemyWeaponThree}</b></h3>
                </div>
                <hr>
                <div><b style="color: red;">FUMBLE</b></div>
                </html>`
            });
        } else {
            attackThree.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: `
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:5px;" src="${this.actor.img}"> <h3 style="border-bottom: none;"><b>${this.actor.name}</b> attacks with their <b>${enemyWeaponThree}</b></h3>
                </div>
                <hr>
                <button class="enemy-damage3" data-dmg="${enemyDamageThree}">Roll Damage</button>
                </html>`
            });
        }
    }

}