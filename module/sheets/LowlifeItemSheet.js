export default class LowlifeItemSheet extends ItemSheet {

    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            width: 530,
            height: 500,
            classes: ["lowlife", "sheet", "item"]
        });
    }

    get template() {
        return `systems/lowlife/templates/sheets/${this.item.type}-sheet.html`;
    }

    getData() {
        const data = super.getData();

        data.config = CONFIG.lowlife;

        return data;
    }
}