export default class LowlifeVehicleSheet extends ActorSheet {

    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            width: 750,
            height: 525,
            classes: ["lowlife", "sheet", "actor", "vehicle"]
        });
    }

    get template() {
        return `systems/lowlife/templates/sheets/actor/${this.actor.type}-sheet.html`;
    }

    getData() {
        const data = super.getData();

        data.config = CONFIG.lowlife;
        data.dtypes = ["String", "Number", "Boolean"];

        if (this.actor.type === 'vehicle') {
            this._prepareCrew(data);
        }

        console.log("LowlifeVehicleSheet.js", "getData", { data });

        return data;
    }

    _prepareCrew(data) {
        data.actor.system.crew.occupants = data.actor.system.crew.occupants.reduce((arr, o) => {
            o.actor = game.actors.get(o.id);
            arr.push(o);
            return arr;
        }, []);
        console.log(data);
        return data;
    }

    //Crew Management
    dropCrew(actorId) {
        console.log(actorId);

        let newId = actorId.slice(6);

        const crew = game.actors.get(newId);
        console.log(crew);

        return this.actor.addVehicleOccupant(newId);
    }

    //Sheet Listeners
    /** @override */
    activateListeners(html) {
        super.activateListeners(html);

        //Crew Management
        html.find('.crew-edit').click(this._onEditCrew.bind(this));
        html.find('.crew-delete').click(this._onDeleteCrew.bind(this));
    }

    _onEditCrew(event) {
        event.preventDefault();
        const element = event.currentTarget;
        const crewId = element.closest('.crew-member').dataset.crewId;
        const actor = game.actors.get(crewId);
        return actor.sheet.render(true);
    }

    _onDeleteCrew(event) {
        event.preventDefault();
        const element = event.currentTarget;
        const crewId = element.closest('.crew-member').dataset.crewId;
        const occupants = this.actor.removeVehicleOccupant(crewId);
        return this.actor.update({ 'system.crew.occupants': occupants });
    }
}