import { spellContent } from "./dialog.js";
import { getGsTf } from "./lowlifeutils.js";

export function chatListeners(html) {
    html.on('click', '.melee-damage', onMeleeDamage);
    html.on('click', '.dragon-damage', onDexDamage);
    html.on('click', '.perc-damage', onPercDamage);
    html.on('click', '.reload', onWeaponReload);
    html.on('click', '.jam-check', onJamCheck);
    html.on('click', '.enemy-damage1', onEnemyDamage1);
    html.on('click', '.enemy-damage2', onEnemyDamage2);
    html.on('click', '.enemy-damage3', onEnemyDamage3);
    html.on('click', '.network-roll', onNetworkRoll);
    html.on('click', '.payment-roll', onPaymentRoll);
    html.on('click', '.spell-check', onSpellCheck);
    html.on('click', '.ability-use', onAbilityUse);
}

async function onMeleeDamage(event) {
    const damage = event.currentTarget.getAttribute('data-dmg');
    const str = event.currentTarget.getAttribute('data-str');
    let dmg = new Roll("@damage + @str", {damage, str});
    await dmg.evaluate();
    dmg.toMessage({
        speaker: ChatMessage.getSpeaker(),
        flavor: "Damage Roll"
    });
}

async function onDexDamage(event){
    const damage = event.currentTarget.getAttribute('data-dmg');
    const dex = event.currentTarget.getAttribute('data-dex');
    let dmg = new Roll("@damage + @dex", {damage, dex});
    await dmg.evaluate;
    dmg.toMessage({
        speaker: ChatMessage.getSpeaker(),
        flavor: "Damage Roll"
    });
}

async function onPercDamage(event){
    const damage = event.currentTarget.getAttribute('data-dmg');
    const perc = event.currentTarget.getAttribute('data-perc');
    let dmg = new Roll("@damage + @perc", {damage, perc});
    await dmg.evaluate();
    dmg.toMessage({
        speaker: ChatMessage.getSpeaker(),
        flavor: "Damage Roll"
    });
}

async function onWeaponReload(event){
    const itemID = event.currentTarget.getAttribute('data-itemId');
    const doc = await fromUuid(event.currentTarget.getAttribute('data-actorId'));
    let actor = doc instanceof Actor ? doc: doc.actor;
    let item = actor.items.get(itemID);

    if (item.system.magNumber > 0) {
        item.update({'system.magCurrent': item.system.magMax});
        item.update({'system.magNumber': item.system.magNumber - 1});
        ChatMessage.create({
            content:`
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:7px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${actor.name}</b> reloaded their ${item.name}.</b></h3>
                <hr>
                </div>
                </html>`
        });
    } else {
        ChatMessage.create({
            content:`
                <html>
                <div style="min-height:50px;"><img style="width:50px; height:50px; float:left; margin-right:5px; margin-top:3px; margin-bottom:7px;" src="${item.img}"> <h3 style="border-bottom: none;"><b>${actor.name}</b> is <b>OUT OF MAGAZINES.</b></h3>
                <hr>
                </div>
                </html>`
        });
    }
}

async function onJamCheck(event) {
    const jam = event.currentTarget.getAttribute('data-jam');
    let itemName = event.currentTarget.getAttribute('data-name');
    let message = "";

    let jamRoll = await new Roll("1d10").evaluate();
    if (jamRoll.total <= jam) {
        message = `<html>
        <div style="min-height:50px;"><h3 style="border-bottom: none;"><b>${itemName} has JAMMED</b></h3><br>
        <div>Clearing a Jam during combat requires a Luck check</div>
        </html>`
    } else {
        message = `<html>
        <div style="min-height:50px;"><h3 style="border-bottom: none;"><b>${itemName} is working properly.</b></h3>
        </html>`
    }

    jamRoll.toMessage({
        speaker: ChatMessage.getSpeaker(),
        flavor: message
    });
}

async function onEnemyDamage1(event){
    const damage = event.currentTarget.getAttribute('data-dmg');
    let rollData = {damage};
    let dmg = new Roll("@damage", rollData);
    await dmg.evaluate();
    dmg.toMessage({
        speaker: ChatMessage.getSpeaker(),
        flavor: "Damage Roll"
    });
}

async function onEnemyDamage2(event){
    const damage = event.currentTarget.getAttribute('data-dmg');
    let rollData = {damage};
    let dmg = new Roll("@damage", rollData);
    await dmg.evaluate();
    dmg.toMessage({
        speaker: ChatMessage.getSpeaker(),
        flavor: "Damage Roll"
    });
}

async function onEnemyDamage3(event){
    const damage = event.currentTarget.getAttribute('data-dmg');
    let rollData = {damage};
    let dmg = new Roll("@damage", rollData);
    await dmg.evaluate();
    dmg.toMessage({
        speaker: ChatMessage.getSpeaker(),
        flavor: "Damage Roll"
    });
}

function onNetworkRoll(event) {
    //get initial info
    const card = event.currentTarget.closest(".contact");
    let lifer = game.actors.get(card.dataset.ownerId);
    let contact = lifer.items.get(card.dataset.itemId);
    let contactValue = contact.system.rating;
    let flavorContent;

    console.log(contactValue);

    //new dialog for modifiers
    const networkDialog = new Dialog({
        title: "Networking Modifiers",
        content: `
        <html>
        <table class="section-table">
        <tr>
            <td><label for="sitMod">Networking Modifier</label></td>
            <td><input type="text" id="sitMod" placeholder="e.g.: -1 or +2" value="0"></td>
        </tr>
        </table>
        </html>`,
        buttons: {
            button1: {
                label: "Roll",
                callback: (html) => netRoll(html)
            }
        },
        default: "button1"
    }).render(true);

    //roll the check
    async function netRoll(html) {
        let sitMod = parseInt(document.getElementById("sitMod").value, 10);
        let r = await new Roll("1d20").evaluate();
        let target = contactValue + sitMod;

        //find success level
        let gs;
        let tf;
        if (target >= 13) {
            gs = Math.floor(target / 2);
            tf = 20;
        } else {
            gs = Math.floor(target / 2);
            tf = Math.floor(target * 1.5);
        }

        if (r.result >= tf) {
            flavorContent = `<html>Networking Check: TERRIBLE FAILURE<hr>${contact.name} is not reachable or is unwilling/able to help. Additionally, an opposing agent might get wind of ${lifer.name}'s inquiries. GM's call or secret LUCK save (don't reduce LUCK).</html>`;
        } else if (r.result > target) {
            flavorContent = `<html>Networking Check: FAILURE<hr>${contact.name} is not reachable or is unwilling/able to help for the duration of this job.</html>`;
        } else if (r.result <= gs) {
            flavorContent = `<html>Networking Check: GREAT SUCCESS<hr>${contact.name} is willing and able to help. They reveal all or most of their useful info on the topic inquired about, or provides the gear/service requested.</html>`;
        } else {
            flavorContent = `<html>Networking Check: SUCCESS<hr>${contact.name} is willing and able to help. They reveal some useful info on the topic inquired about, or provides the partial or similar gear/service to that requested.</html>`;
        }

        r.toMessage({
            speaker: ChatMessage.getSpeaker(),
            flavor: flavorContent
        });
    }
}

function onPaymentRoll(event) {
    //get initial info
    const card = event.currentTarget.closest(".contact");
    let lifer = game.actors.get(card.dataset.ownerId);
    let contact = lifer.items.get(card.dataset.itemId);
    let chaValue = lifer.system.attributes.cha.value;
    let flavorContent;

    //new dialog for modifiers
    const networkDialog = new Dialog({
        title: "Payment Roll Modifiers",
        content: `
        <html>
        <table class="section-table">
        <tr>
            <td><label for="sitMod">Payment Roll Modifier</label></td>
            <td><input type="text" id="sitMod" placeholder="e.g.: -1 or +2" value="0"></td>
        </tr>
        </table>
        </html>`,
        buttons: {
            button1: {
                label: "Roll",
                callback: (html) => payRoll(html)
            }
        },
        default: "button1"
    }).render(true);

    //roll the check
    async function payRoll(html) {
        let sitMod = parseInt(document.getElementById("sitMod").value, 10);
        let r = await new Roll("1d20").evaluate();
        let target = chaValue + sitMod;

        //find success level
        let gs;
        let tf;
        if (target >= 13) {
            gs = Math.floor(target / 2);
            tf = 20;
        } else {
            gs = Math.floor(target / 2);
            tf = Math.floor(target * 1.5);
        }

        if (r.result >= tf) {
            flavorContent = "Payment Check: TERRIBLE FAILURE<hr>3d6 x 100 ceracoin for intel. 3d6 x 1,000 ceracoin for a service or to borrow gear. Also, a major favour is owed.";
        } else if (r.result > target) {
            flavorContent = "Payment Check: FAILURE<hr>3d6 x 60 ceracoin for intel. 3d6 x 600 ceracoin for a service or to borrow gear. Also, a favour will be called in at a future time.";
        } else if (r.result <= gs) {
            flavorContent = "Payment Check: GREAT SUCCESS<hr>1d6 x 10 ceracoin for intel. 3d6 x 100 ceracoin for a service or to borrow gear.";
        } else {
            flavorContent = "Payment Check: SUCCESS<hr>3d6 x 30 ceracoin for intel. 3d6 x 300 ceracoin for a service or to borrow gear. Alternatively, a favour to be called in at a future time.";
        }

        r.toMessage({
            speaker: ChatMessage.getSpeaker(),
            flavor: flavorContent
        });
    }
}

async function onSpellCheck(event) {
    //Get Lifer info
    const card = event.currentTarget.closest(".spell");
    const doc = "Actor."+card.dataset.ownerId;
    let actor = await fromUuid(doc);
    
    const spellDialog = new Dialog({
        title: "Spell Casting Modifiers",
        content: spellContent,
        buttons: {
            button1: {
                label: "Cast",
                callback: (html) => castSpell(html)
            }
        },
        default: "button1"
    }).render(true);

    async function castSpell(html) {
        let sitMod = html.find("input#sitMod").val();
        let message = "";

        //Get Will score and resulting Great Success / Terrible Failure
        let will = actor.system.attributes.will.value;
        let { gs, tf } = getGsTf(will);
        console.log("GS: ", gs, " TF: ", tf);
        
        //Roll the Spell Check
        let spellCast = await new Roll("1d20 + @sitMod", {sitMod}).evaluate();
        if (spellCast.total >= tf) {
            message = "Spell fails. Caster is drained of 1 Will. Roll on the Dark Flux table with Disadvantage (GM's choice).";
            actor.update({'system.attributes.will.value': actor.system.attributes.will.value - 1});
        } else if (spellCast.total > will) {
            message = "Spell fails. Caster is drained of 1 Will. Roll on the Dark Flux table.";
            actor.update({'system.attributes.will.value': actor.system.attributes.will.value - 1});
        } else if (spellCast.total > gs) {
            message = "Spell works as intended. Caster is drained of 1 Will.";
            actor.update({'system.attributes.will.value': actor.system.attributes.will.value - 1});
        } else if (spellCast.total <= tf) {
            message = "Spell is extra potent (see Spell description). Caster is drained of 1 Will.";
            actor.update({'system.attributes.will.value': actor.system.attributes.will.value - 1});
        }

        spellCast.toMessage({
            speaker: ChatMessage.getSpeaker(),
            flavor: message
        });
    }
}

async function onAbilityUse(event) {
    //Get Lifer and info
    const card = event.currentTarget.closest(".ability");
    const doc = "Actor."+card.dataset.ownerId;
    let actor = await fromUuid(doc);
    const item = actor.items.get(card.dataset.itemId);
    
    item.update({'system.useVal': item.system.useVal - 1});
}