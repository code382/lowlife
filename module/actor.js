/**
 * Extend base Actor entity by defining custom roll data structure.
 * @extends {Actor}
 */

import { getGsTf } from "../module/lowlifeutils.js";

export class LowlifeActor extends Actor {

    /**
     * Additional Dynamic Data
     */

    prepareData() {

        super.prepareData();

        const actorData = this;
        const data = actorData.system;
        const flags = actorData.flags;

        if (actorData.type === 'lifer') this._prepareLiferData(actorData);
        if (actorData.type === 'npc') this._prepareNpcData(actorData);
        if (actorData.type === 'adversary') this._prepareAdversaryData(actorData);
        if (actorData.type === 'boss') this._prepareBossData(actorData);
        if (actorData.type === 'vehicle') this._prepareVehicleData(actorData);
    }

    _prepareLiferData(actorData) {
        const data = actorData.system;
        data.initiative = Math.floor(((data.attributes.dex.value + data.attributes.int.value) / 2) + data.initMod);

        //Looping through attribute scores for modifiers
        for (let [key, attribute] of Object.entries(data.attributes)) {
            if (attribute.value >= 19) { attribute.mod = 4; }
            else if (attribute.value >= 17) { attribute.mod = 3; }
            else if (attribute.value >= 15) { attribute.mod = 2; }
            else if (attribute.value >= 13) { attribute.mod = 1; }
            else if (attribute.value >= 9) { attribute.mod = 0; }
            else if (attribute.value >= 7) { attribute.mod = -1; }
            else if (attribute.value >= 5) { attribute.mod = -2; }
            else if (attribute.value >= 3) { attribute.mod = -3; }
            else { attribute.mod = -4; }

            getGsTf(attribute.value);
        }

        //Find all "armour" items and add AR
        const equippedArmour = actorData.items.filter(function(item) {
            return item.system.equipped;
        });
        console.log(equippedArmour);

        const armourRating = equippedArmour.reduce((cur, item) => {
            return cur + item.system.ar;
        }, 0);
        console.log(armourRating);
        data.ar = armourRating;
    }

    _prepareNpcData(actorData) {
        const data = actorData.system;

        //Looping through attribute scores for modifiers
        for (let [key, attribute] of Object.entries(data.attributes)) {
            if (attribute.value >= 19) { attribute.mod = 4; }
            else if (attribute.value >= 17) { attribute.mod = 3; }
            else if (attribute.value >= 15) { attribute.mod = 2; }
            else if (attribute.value >= 13) { attribute.mod = 1; }
            else if (attribute.value >= 9) { attribute.mod = 0; }
            else if (attribute.value >= 7) { attribute.mod = -1; }
            else if (attribute.value >= 5) { attribute.mod = -2; }
            else if (attribute.value >= 3) { attribute.mod = -3; }
            else { attribute.mod = -4; }

            getGsTf(attribute.value);
        }
    }

    _prepareAdversaryData(actorData) {
        const data = actorData.system;

        //Looping through attribute scores for modifiers
        for (let [key, attribute] of Object.entries(data.attributes)) {
            if (attribute.value >= 19) { attribute.mod = 4; }
            else if (attribute.value >= 17) { attribute.mod = 3; }
            else if (attribute.value >= 15) { attribute.mod = 2; }
            else if (attribute.value >= 13) { attribute.mod = 1; }
            else if (attribute.value >= 9) { attribute.mod = 0; }
            else if (attribute.value >= 7) { attribute.mod = -1; }
            else if (attribute.value >= 5) { attribute.mod = -2; }
            else if (attribute.value >= 3) { attribute.mod = -3; }
            else { attribute.mod = -4; }

            getGsTf(attribute.value);
        }
    }

    _prepareBossData(actorData) {
        const data = actorData.system;
        
        //Looping through attribute scores for modifiers
        for (let [key, attribute] of Object.entries(data.attributes)) {
            if (attribute.value >= 19) { attribute.mod = 4; }
            else if (attribute.value >= 17) { attribute.mod = 3; }
            else if (attribute.value >= 15) { attribute.mod = 2; }
            else if (attribute.value >= 13) { attribute.mod = 1; }
            else if (attribute.value >= 9) { attribute.mod = 0; }
            else if (attribute.value >= 7) { attribute.mod = -1; }
            else if (attribute.value >= 5) { attribute.mod = -2; }
            else if (attribute.value >= 3) { attribute.mod = -3; }
            else { attribute.mod = -4; }

            getGsTf(attribute.value);
        }
    }

    _prepareVehicleData(actorData) {
        const data = actorData.system;
    }

    //Vehicle Crew Management
    //Adding Occupant
    addVehicleOccupant(crewId) {
        if (this.type != 'vehicle') return;
        const system = this.system;
        let occupant = {
            id: crewId
        };

        console.log("TESTING: ", system.crew);
        //Remove Duplicates
        if (system.crew.occupants.some(o => o.id === crewId)) this.removeVehicleOccupant(crewId);
        //Adds New Occupant
        system.crew.occupants.push(occupant);
        this.update({'system.crew.occupants': system.crew.occupants});
        console.log(occupant);
        return occupant;
    }

    //Removing Occupant
    removeVehicleOccupant(crewId) {
        if (this.type != 'vehicle') return;
        const crew = this.system.crew;
        crew.occupants = crew.occupants.filter(o => o.id !== crewId);
        return crew.occupants;
    }

    //Get Specific Occupant
    getVehicleOccupant(crewId) {
        if (this.type !== 'vehicle') return;
        return this.system.occupants.find(o => o.id === crewId);
    }

}