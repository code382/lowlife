/**
 * Extend base Item entity
 * @extends {Item}
 */

 export class LowlifeItem extends Item {

    chatTemplate = {
        "weapon": "systems/lowlife/templates/chat/weapon-chat.hbs",
        "vehicle": "systems/lowlife/templates/chat/vehicle-chat.hbs",
        "spell": "systems/lowlife/templates/chat/spell-chat.hbs",
        "armour": "systems/lowlife/templates/chat/armour-chat.hbs",
        "drug": "systems/lowlife/templates/chat/drug-chat.hbs",
        "toxin": "systems/lowlife/templates/chat/toxin-chat.hbs",
        "gear": "systems/lowlife/templates/chat/gear-chat.hbs",
        "contact": "systems/lowlife/templates/chat/contact-chat.hbs",
        "ability": "systems/lowlife/templates/chat/ability-chat.hbs",
        "setback": "systems/lowlife/templates/chat/setback-chat.hbs"
    }

    async rollItem() {
        let chatData = {
            user: game.user._id,
            speaker: ChatMessage.getSpeaker()
        };

        let cardData = {
            ...this,
            id: this.id,
            owner: this.actor.id
        };

        chatData.content = await renderTemplate(this.chatTemplate[this.type], cardData);

        chatData.rollItem = true;
        
        return ChatMessage.create(chatData);
    }
 }