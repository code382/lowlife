import { lowlife } from "./module/config.js";
import * as Chat from "./module/chat.js";
import { LowlifeActor } from "./module/actor.js";
import { LowlifeItem } from "./module/item.js";
import LowlifeItemSheet from "./module/sheets/LowlifeItemSheet.js";
import LowlifeActorSheet from "./module/sheets/LowlifeActorSheet.js";
import LowlifeVehicleSheet from "./module/sheets/LowlifeVehicleSheet.js"
import { lowlifeRoll } from "./module/lowlifeutils.js";

Hooks.once("init", function () {
    console.log("lowlife | Initializing Lowlife 2090 System");

    game.lowlife = {
        LowlifeActor,
        LowlifeItem,
        lowlifeRoll,
        rollItemMacro
    }

    CONFIG.lowlife = lowlife;
    CONFIG.Actor.documentClass = LowlifeActor;
    CONFIG.Item.documentClass = LowlifeItem;

    Items.unregisterSheet("core", ItemSheet);
    Items.registerSheet("lowlife", LowlifeItemSheet, { makeDefault: true });

    Actors.unregisterSheet("core", ActorSheet);
    Actors.registerSheet("lowlife", LowlifeActorSheet, {
        types: ["lifer", "npc", "adversary", "boss"],
        makeDefault: true
    });
    Actors.registerSheet("lowlife", LowlifeVehicleSheet, {
        types: ["vehicle"],
        makeDefault: true
    });

    Handlebars.registerHelper('isPerc', function(value) {
        let check = false;
        if (value == "perc") {
            console.log("Weapon is Ranged");
            return check = true;
        } else {
            console.log("Weapon is Melee");
        }
    });

});

Hooks.once("ready", async function() {
    //Waiting to register hotbar drop hook on reaky so that modules can register earlier if needed
    Hooks.on("hotbarDrop", (bar, data, slot) => createLowlifeMacro(data, slot));
});

// Hotbar Macros

/**
 * Create Macro from Item Drop.
 * Get existing item macro if one exists, else create new
 * @param {Object} data     Dropped Data
 * @param {number} slot     Hotbar Slot
 * @returns {Promise}
 */

async function createLowlifeMacro(data, slot) {
    if (data.type !== "Item") return;
    if (!("data" in data)) return ui.notifications.warn("You can only create macro buttons for Owned Items");
    const item = data.data;

    //Create Macro Command
    const command = `game.lowlife.rollItemMacro("${item.name}");`;
    let macro = game.macros.entities.find(m => (m.name === item.name) && (m.command === command));
    if (!macro) {
        macro = await Macro.create({
            name: item.name,
            type: "script",
            img: item.img,
            command: command,
            flags: { "lowlife.itemMacro": true}
        });
    }
    game.user.assignHotbarMacro(macro, slot);
    return false;
}

/**
 * Create Macro from Item Drop
 * Get existing item macro if one exists, else create new
 * @param {string} itemName
 * @return {Promise}
 */

function rollItemMacro(itemName) {
    const speaker = ChatMessage.getSpeaker();
    let actor;
    if (speaker.token) actor = game.actors.tokens[speaker.token];
    if (!actor) actor = game.actors.get(speaker.actor);
    const item = actor ? actor.items.find(i => i.name === itemName) : null;
    if (!item) return ui.notifications.warn(`Your controlled Actor does not have an item named ${itemName}`);
  
    // Trigger the item roll
    return item.roll();
  }

//Creating Chat Listeners
Hooks.on("renderChatLog", (app, html, data) => Chat.chatListeners(html));

//Dropping Actors onto Vehicles
Hooks.on('dropActorSheetData', (actor, sheet, data) => {
    //When dropping something on a vehicle sheet.
    if (actor.type === 'vehicle') {
        //Dropping an actor on a vehicle sheet.
        if (data.type === 'Actor') { sheet.dropCrew(data.uuid); }
    }
});