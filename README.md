# Lowlife 2090

System development for FoundryVTT compatibility with the Lowlife 2090 TTRPG written by Stephen J. Grodzicki.

This version of the Lowlife 2090 system for Foundry VTT has been coded with permission for distribution from Pickpocket Press. It includes a character sheet and system support for Lowlife 2090.

Any questions, comments, concerns, etc. may be emailed to development.devin@gmail.com or communicated through gitlab.

NEW DISCORD: https://discord.gg/ZvGfKPrRmy  Come join us to talk about future updates and current bugs. In all honesty, it's checked a bit more than the GitLab.

## Version 1.12.3

Update Includes:

- Bug fix: changes on NPC, Adversary, Boss sheets were not persisting on close

## Version 1.12.2

Update Includes:

- Initiative bug fix

## Version 1.12.1

Update Includes:

- FVTT v12 compatibility

## Version 1.11.2

Update Includes:

- Fixed random chat button resizing when a dialog is opened.
- Added short rest automation, where clicking on the "Short Rest" button in the Lifer Sheet left sidebar will auto-roll the two WILL checks and count successes. The chat will then tell you how many recoveries you can make and what they are.
- Added long rest automation, where clicking on the "Long Rest" button will automatically increase the Lifer's current HP by one if they are below Max, will automatically uncheck the "Fatigued" condition if it is checked, and will roll 2 WILL checks as above.
- Added an "Equipped" checkbox to Armour items. AR is now non-editable and can only be changed through "equipping" Armour items, whose AR values will be summed if they are "equipped".
- Added Ammo Tracking and Reloading automation. Single shot subtracts 1, burst subtracts 3, and full auto subtracts 10 (unless modified). If you do not have enough bullets, a message will appear in chat asking you to reload. You can also choose to reload using the Dialog to fire so that you don't need to open the sheet and manually edit the numbers.
- Added "Extra Bullets" input for the rule that "every 2 bullets fired above the standard 10 increases frontage by 1m." This frontage is updated in the resulting chat message, and 10+"Extra" are subtracted from the current magazine.
- Added Jam automation. On a fumble, the chat message will have a button for a Jam Check, which will roll and compare against your weapon's Jam input. **NOTE: For this automation to work, only enter the highest number that the weapon will Jam on (3 instead of 1-3).**
- Lastly for Guns, Specialty Ammo is now rollable. You will enter in the *adjusted* values (e.g.: a 4 in the Jam input for caseless when a normal pistol might be a 3), and everything will be rolled and tracked as above. I've also edited how Specialty Ammo shows up on the Lifer sheet for readability.
- Added a "Cast Spell" button to the bottom of a Spell chat card. This automatically makes the Spell Check, drains WILL, and sends the level of success to the chat.
- Added a "Use Ability" button to the bottom of an Ability chat card, which just subtracts 1 from the Uses Remaining for that ability.
- Added a dialog for Attribute and Luck checks. This allows for modified rolls and advantage/disadvantage without having to remember a key-binding.
- Added a Vehicle actor type so that Drones and Vehicles can be added to the battle map. These have the added functionality of being able to drag n drop actors "into" the vehicle sheet. There, you will see their name if you hover over them, and you can open their sheet from the vehicle sheet to make checks.

## Version 1.11.1

Update Includes:

- v11 Compatibility

## Version 1.10.2

Update Includes:

- Full v10 migration

## Version 1.10.1

Update Includes:

- Fix to chat card layout.
- Initial move to v10, including new Version number system which will follow Foundry system version (1.10.1 will be first update for FVTT v10)

## Version 1.0.9

Update Includes:

- Automation for Networking checks. Click on the name for the contact and it sends it to the chat log with additional buttons for networking and payment checks
- Fixed compatability issue with PopOut! module where a popped out sheet could not roll attacks
- All items/setbacks are able to be sent to chat. All have their own chat card with necessary information displayed

## Version 1.0.8

Update Includes:

- Changes for use with v9 of FoundryVTT

## Version 1.0.7

Update includes

- Tested compatibility for 0.8.9
- Early updating for v9
- Drag and Drop reordering for Items
- "Shift + Click" for rolling attribute, initiative, and luck checks *with advantage* from sheet. "Alt + Click" for *disadvantage*

## Version 1.0.6

Update includes

- Added Heavy to Adversary Sheet and Car Crusher to both Adversary and Boss Sheets
- Added Level to Lifer Sheet
- Changed Disguise skill to Disguise and Forgery
- Added Class Abilities tally (4 Types: 1 Class and 3 Crossclass) to Lifer Sheet
- Tweaked ammo section on Weapons Sheet
- Added Special Ammo and associated Sheet and Table (in Combat tab)

## Version 1.0.5

NOTE: This version requires a minimum version of FoundryVTT 0.8.5. If you need a previous version, create an issue or reach out on Discord.

Update includes

- Testing for compatability with v0.8.8 of FoundryVTT
- Items and Weapons to chat, including dialogs for both melee and ranged attacks as well as the ability to roll associated damage from the chat card.
- Added DEX to the Attack Stat dropdown for weapons.
- Updated Skills to match the final ruleset.
- Adversary and Boss attacks have been upped to 3 (with associated damages) to account for enemies carrying multiple weapons. These are also rollable from the sheet with damage rollable from the chat card.
- Added Ceracoin (money) into the Social Tab and adjusted the Heat Rating column size
- Added a Supply Type to Items in the form of a dropdown with abbreviations

## Version 1.0.4

Fixed bug with users not being able to see item info in character sheet.

## Version 1.0.3

This contains a quick update to ensure compatibility with the 0.8.x Foundry structure.

## Version 1.0.0

This is the base system for Lowlife 2090. It includes rollable stats, automatically decrementing Luck rolls, and all sheets for storing actor and item data. It is based off of the playtest version of Lowlife 2090, currently out to Kickstarters. This system will evolve along with Lowlife 2090 until (and after) its release. Future updates will include:

- Rollable Items with Chat Cards (currently testing, not available in base system)
- Active Effects
- Polyglot Module Support
- Compendiums (once the item types are finalized in the ruleset)
- Other Updates as requested

## Icons Used

Icons used in this system are provided by https://game-icons.net and their amazing contributors.

## Note on Initiative

Initiative in Lowlife 2090 is made through a "Lifer" making a successful Initiative Check, sometimes by degree of success, on each round of combat. If rolling from a Token, the check is purely "success" (counted as 1) or "failure" (counted as 0). If a degree of success is needed, roll from the Character Sheet.

## Manual Installation

Simply download the .zip file and extract it into Foundry's Data/systems/lowlife folder. After extracting, make sure the name of the containing folder is set to 'lowlife' and not 'lowlife-master'.
